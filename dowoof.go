package main

import (
	"fmt"
	"os/exec"
	"sync"
	"time"
)

var woofMutex sync.Mutex

func woof() {
	woofMutex.Lock()
	w := exec.Command("speak", "woof")
	running := false
	go func() {
		time.Sleep(10 * time.Second)
		if running {
			e := w.Process.Kill()
			if e != nil {
				fmt.Println(e)
			}
		}
	}()
	running = true
	b, e := w.CombinedOutput()
	if e != nil {
		fmt.Println(e)
		fmt.Println(string(b))
	}
	running = false
	woofMutex.Unlock()
}
