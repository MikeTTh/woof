package main

import "net/http"

func WoofHandler(w http.ResponseWriter, r *http.Request) {
	if r.URL.Path == "/woof.mp3" {
		http.ServeFile(w, r, "woof.mp3")
		return
	}

	if r.URL.Path == "/woof" {
		woof()
		return
	}

	http.ServeFile(w, r, "index.html")
}
