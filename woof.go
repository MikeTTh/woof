package main

import (
	"fmt"
	"net/http"
)

func main() {
	e := http.ListenAndServe(":8080", http.HandlerFunc(WoofHandler))
	if e != nil {
		fmt.Println(e)
	}
}
