FROM archlinux:latest

WORKDIR /app

ADD . /app

RUN pacman --noconfirm -Sy go espeak-ng-espeak && go build -o ./app .

EXPOSE 8080

ENTRYPOINT ["/app/app"]